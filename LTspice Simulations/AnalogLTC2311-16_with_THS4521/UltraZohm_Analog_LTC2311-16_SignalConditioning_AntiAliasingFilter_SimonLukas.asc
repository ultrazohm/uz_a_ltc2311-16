Version 4
SHEET 1 2832 1332
WIRE 384 0 336 0
WIRE 608 0 448 0
WIRE 64 48 -176 48
WIRE -992 80 -1072 80
WIRE -848 80 -912 80
WIRE -736 80 -848 80
WIRE 336 80 336 0
WIRE 448 80 336 80
WIRE 608 80 608 0
WIRE 608 80 528 80
WIRE 992 112 992 96
WIRE -176 128 -176 48
WIRE -144 128 -176 128
WIRE 64 144 64 48
WIRE 64 144 -80 144
WIRE 160 144 64 144
WIRE 336 144 336 80
WIRE 336 144 240 144
WIRE -400 160 -480 160
WIRE -256 160 -320 160
WIRE -144 160 -256 160
WIRE -256 176 -256 160
WIRE 592 192 512 192
WIRE 608 192 608 80
WIRE 736 192 608 192
WIRE 880 192 736 192
WIRE 992 192 992 176
WIRE 992 192 960 192
WIRE 1152 192 992 192
WIRE 352 208 320 208
WIRE 992 208 992 192
WIRE 528 224 512 224
WIRE -1072 240 -1072 80
WIRE 336 240 336 144
WIRE 352 240 336 240
WIRE -256 256 -256 240
WIRE 528 256 512 256
WIRE 64 272 -176 272
WIRE 160 272 64 272
WIRE 320 272 320 208
WIRE 320 272 240 272
WIRE 352 272 336 272
WIRE -928 288 -1072 288
WIRE 528 288 512 288
WIRE 592 288 592 192
WIRE 736 288 592 288
WIRE 880 288 736 288
WIRE 992 288 992 272
WIRE 992 288 960 288
WIRE 1152 288 992 288
WIRE 992 304 992 288
WIRE -1072 320 -1072 288
WIRE 528 320 528 288
WIRE -176 352 -176 272
WIRE -144 352 -176 352
WIRE 64 368 64 272
WIRE 64 368 -80 368
WIRE 336 368 336 272
WIRE 608 368 608 192
WIRE 608 368 336 368
WIRE -400 384 -480 384
WIRE -256 384 -320 384
WIRE -144 384 -256 384
WIRE 992 384 992 368
WIRE -256 400 -256 384
WIRE 320 416 320 272
WIRE 448 416 320 416
WIRE 592 416 592 288
WIRE 592 416 528 416
WIRE -1072 432 -1072 400
WIRE -256 480 -256 464
WIRE 320 496 320 416
WIRE 368 496 320 496
WIRE 592 496 592 416
WIRE 592 496 432 496
WIRE 1072 560 880 560
WIRE 1104 560 1072 560
WIRE 1440 560 1392 560
WIRE 1488 560 1440 560
WIRE 1520 560 1488 560
WIRE 880 576 880 560
WIRE 1440 576 1440 560
WIRE 1520 576 1520 560
WIRE 1072 656 1072 560
WIRE 1104 656 1072 656
WIRE 1440 656 1440 640
WIRE 1440 656 1392 656
WIRE 1520 656 1520 640
WIRE 880 672 880 656
WIRE 1248 752 1248 720
WIRE 1408 864 1392 864
WIRE 1440 864 1408 864
WIRE 1504 864 1440 864
WIRE 1520 864 1504 864
WIRE 1072 880 880 880
WIRE 1104 880 1072 880
WIRE 880 896 880 880
WIRE 1440 896 1440 864
WIRE 1520 896 1520 864
WIRE 1408 928 1408 864
WIRE 1408 928 1392 928
WIRE 272 976 272 944
WIRE 416 976 416 944
WIRE 1072 976 1072 880
WIRE 1104 976 1072 976
WIRE 1520 976 1520 960
WIRE 880 992 880 976
WIRE 1440 992 1440 960
WIRE 1440 992 1392 992
WIRE 1248 1072 1248 1040
WIRE 1264 1136 1200 1136
FLAG 880 992 0
FLAG 1520 976 0
FLAG 1248 1072 0
FLAG 1504 864 5V
FLAG 880 672 0
FLAG 1520 656 0
FLAG 1248 752 0
FLAG 1488 560 2.5V
FLAG 736 192 V_outADCdriver+
FLAG 736 288 V_outADCdriver-
FLAG 992 96 0
FLAG 992 384 0
FLAG 1152 192 V_ADCin+
FLAG 1152 288 V_ADCin-
FLAG -112 112 5V
FLAG -112 176 -Vs
FLAG -112 336 5V
FLAG 416 976 0
FLAG 64 272 Buffer+
FLAG -112 400 -Vs
FLAG 416 864 Vin+
FLAG 272 864 Vin-
FLAG -848 240 0
FLAG -1072 240 0
FLAG -480 384 Vin+
FLAG -1072 432 0
FLAG -480 160 Vin-
FLAG -1072 928 0
FLAG -1072 784 0
FLAG 272 976 0
FLAG 1200 1136 -Vs
FLAG 1264 1216 0
FLAG 64 144 Buffer-
FLAG 528 256 5V
FLAG 528 320 0
FLAG 528 224 2.5V
FLAG -256 256 0
FLAG -256 480 0
SYMBOL voltage 880 880 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 9
SYMBOL cap 1504 896 R0
SYMATTR InstName C3
SYMATTR Value 10�
SYMBOL cap 1424 896 R0
SYMATTR InstName C4
SYMATTR Value .01�
SYMBOL PowerProducts\\LT1763-5 1248 928 R0
SYMATTR InstName U1
SYMBOL voltage 880 560 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V3
SYMATTR Value 9
SYMBOL cap 1504 576 R0
SYMATTR InstName C5
SYMATTR Value 10�
SYMBOL cap 1424 576 R0
SYMATTR InstName C6
SYMATTR Value .01�
SYMBOL res 544 64 R90
WINDOW 0 3 61 VBottom 2
WINDOW 3 31 61 VTop 2
SYMATTR InstName R1
SYMATTR Value {Rfilt}
SYMBOL res 544 400 R90
WINDOW 0 1 58 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value {Rfilt}
SYMBOL res 976 176 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R5
SYMATTR Value 24.9
SYMBOL res 976 272 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R6
SYMATTR Value 24.9
SYMBOL cap 976 208 R0
WINDOW 0 55 21 Left 2
WINDOW 3 36 44 Left 2
SYMATTR InstName C7
SYMATTR Value {Cdiff}
SYMBOL cap 976 112 R0
WINDOW 0 53 23 Left 2
WINDOW 3 36 43 Left 2
SYMATTR InstName C8
SYMATTR Value {Ccm}
SYMBOL cap 976 304 R0
WINDOW 0 55 23 Left 2
WINDOW 3 37 45 Left 2
SYMATTR InstName C9
SYMATTR Value {Ccm}
SYMBOL cap 432 480 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C10
SYMATTR Value 56p
SYMBOL cap 448 -16 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C11
SYMATTR Value 56p
SYMBOL voltage 416 848 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value2 AC 1
SYMATTR InstName V4
SYMATTR Value 2.5
SYMBOL PowerProducts\\LT1761-2.5 1248 608 R0
SYMATTR InstName U4
SYMBOL res 256 256 R90
WINDOW 0 1 57 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value {Rfilt}
SYMBOL res 256 128 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R7
SYMATTR Value {Rfilt}
SYMBOL Opamps\\LT6204 -112 144 R0
WINDOW 0 22 -16 Left 2
WINDOW 3 24 16 Left 2
SYMATTR InstName U2
SYMBOL Opamps\\LT6204 -112 368 R0
WINDOW 0 22 -16 Left 2
WINDOW 3 24 16 Left 2
SYMATTR InstName U3
SYMBOL voltage -1072 688 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V5
SYMATTR Value PWL(5u 2 10u 2.4 15u 2.4 20u 2.5 25u 2.5 30u -2 35u -2 40u -2.5)
SYMBOL voltage -1072 832 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V9
SYMATTR Value PWL(5u 0 10u 3 15u 3 20u 4.5 25u 4.5 30u 4.75 35u 4.75 40u 5)
SYMBOL current -992 80 R270
WINDOW 0 32 40 VTop 2
WINDOW 3 55 39 VTop 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName I1
SYMATTR Value SINE(0 0.003 50k)
SYMBOL res -864 64 R0
SYMATTR InstName R2
SYMATTR Value 820
SYMBOL voltage -848 144 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value2 AC 1V
SYMATTR InstName V10
SYMATTR Value SINE(2.5 0 50k)
SYMBOL voltage -1072 304 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V11
SYMATTR Value 2.5
SYMBOL voltage 272 848 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V6
SYMATTR Value 2.5
SYMBOL voltage 1264 1120 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V8
SYMATTR Value -5
SYMBOL THS4521 432 240 R0
SYMATTR InstName U5
SYMBOL res -304 144 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R8
SYMATTR Value 100
SYMBOL res -304 368 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R9
SYMATTR Value 100
SYMBOL cap -272 176 R0
SYMATTR InstName C1
SYMATTR Value 100p
SYMBOL cap -272 400 R0
SYMATTR InstName C2
SYMATTR Value 100p
TEXT 248 688 Left 2 ;.tran 0 0.00005 0
TEXT 216 648 Left 2 !.ac dec 1000 1k 10Meg
TEXT -448 -168 Left 4 ;Simulierte Ausgangsspannung\nStromsensor von 0-5V
TEXT 144 760 Left 2 !.param Rfilt=1k Ccm=18n Cdiff=12n
